class StaticPagesController < ApplicationController
  def home
  end

  def form
    response.headers.delete "X-Frame-Options"
  end

  def iframe
    require 'rubygems'
    require 'nokogiri'
    require 'open-uri'

    @page = Nokogiri::HTML(open(params[:url]))
    @header = @page.css('body *').css('*:has(#header), #header')
    @page.css('body').remove
    body = Nokogiri::XML::Node.new 'body', @page
    body.children = @header
    @page.css('html').children.last.add_previous_sibling body

    @page.css('link[rel=stylesheet]').each do |link|
      val = link.attributes["href"].value
      unless val.start_with?('http')
        link.attributes["href"].value = (params[:url].end_with?('/') ? params[:url] : params[:url] + '/') + (val.start_with?('/') ? val[1..-1] : val)
      end
    end

    @page.css('script[src]').each do |link|
      val = link.attributes["src"].value
      unless val.start_with?('http')
        link.attributes["src"].value = (params[:url].end_with?('/') ? params[:url] : params[:url] + '/') + (val.start_with?('/') ? val[1..-1] : val)
      end
    end

    @page.css('a').each do |link|
      link['target'] = '_top'
    end

    render layout: false
  end
end
