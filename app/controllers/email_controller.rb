class EmailController < ApplicationController
  def send_email
    Mailer.send_email_with_form(params[:email], params['subject'], params['message_text']).deliver_now
    respond_to do |format|
      format.js {

      }
      format.html {
        url = params[:partner_site_url]
        redirect_to controller: :static_pages, action: :home, url: url, mail_to: params[:email]
      }
    end
  end
end
