class Mailer < ApplicationMailer
  default from: "fromrussia28@gmail.com"
  def send_email_with_form(email, subject, text)
    @text = text
    mail(to: email, subject: subject)
  end
end
